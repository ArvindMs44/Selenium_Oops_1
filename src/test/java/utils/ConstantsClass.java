package utils;

public class ConstantsClass {

    public static String baseUrl_LoginPage = "https://rahulshettyacademy.com/loginpagePractise/#";
    public static String expectedTitle_LoginPage = "LoginPage Practise | Rahul Shetty Academy";
    public static String expectedTitle_HomePage = "ProtoCommerce";
    public static String baseUrl_HomePage = "https://rahulshettyacademy.com/angularpractice/shop";
    public static String expectedText_HomePage = "Checkout ( 1 )";
    public static String expectedSuccessMsg_CheckoutPage = "Success! Thank you! Your order will be delivered in next few weeks :-).";

}